﻿using System;
using Android.App;
using Android.OS;
using Android.Content;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using System.Collections.Generic;
using Android.Graphics;
using System.Threading.Tasks;

namespace Kiosko
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    [IntentFilter(new[] { Intent.ActionMain }, Categories = new[] { Intent.CategoryHome, Intent.CategoryDefault })]

    public class MainActivity : AppCompatActivity
    {
        private String pinCode = "1111";
        String launcher = String.Empty;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.activity_main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            Button btn = FindViewById<Button>(Resource.Id.button1);

            btn.Click += BtnOnClick;


            //HideNotificationBar();
        }
        public class customViewGroup : ViewGroup
        {
            public customViewGroup(Context context) : base(context)
            {
            }
            public override bool OnTouchEvent(MotionEvent ev)
            {
                return true;
            }
            protected override void OnLayout(bool changed, int l, int t, int r, int b)
            {
                // throw new NotImplementedException();
            }
        }
    

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void BtnOnClick(object sender, EventArgs eventArgs)
        {
            EditText edt = FindViewById<EditText>(Resource.Id.text);
            if (edt.Text.Equals(pinCode))
            {
                LauncherForDefault();
                //StopLockTask();
            }
            else
            {
                Toast.MakeText(this, "Pin incorrecto", ToastLength.Long).Show();
            }

        }

        public void HideNotificationBar()
        {
            /*if (Build.VERSION.SdkInt > BuildVersionCodes.JellyBean)
            {

                this.Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
            }*/

            WindowManagerLayoutParams localLayoutParams = new WindowManagerLayoutParams();

            localLayoutParams.Type = WindowManagerTypes.StatusBarPanel;
            localLayoutParams.Gravity = GravityFlags.Top;
            localLayoutParams.Flags = WindowManagerFlags.NotFocusable |
                WindowManagerFlags.NotTouchModal | WindowManagerFlags.LayoutInScreen;
            localLayoutParams.Width = WindowManagerLayoutParams.MatchParent;
            localLayoutParams.Height = (int)(50 * Resources.DisplayMetrics.ScaledDensity);
            localLayoutParams.Format = Format.Transparent;

            IWindowManager manager = ApplicationContext.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();
            customViewGroup view = new customViewGroup(this);

            manager.AddView(view, localLayoutParams);
        }

        public override void OnBackPressed()
        {
            // Nada para que al pulsar el boton no haga ninguna accion
        }

        protected override void OnResume()
        {
            base.OnResume();
            //StartLockTask();
            PackageManager.AddPackageToPreferred(PackageName);
        }

        protected override void OnStart()
        {
            base.OnStart();
            //StartLockTask();
            PackageManager.AddPackageToPreferred(PackageName);
        }

        private String isMyAppLauncherDefault()
        {
            Intent intent = new Intent(Intent.ActionMain);
            intent.AddCategory(Intent.CategoryHome);
            ResolveInfo resolveInfo = PackageManager.ResolveActivity(intent, PackageInfoFlags.MatchDefaultOnly);
            String currentHomePackage = resolveInfo.ActivityInfo.PackageName;

            return currentHomePackage;
        }

        private void ListAllLaunchers()
        {
            Intent intent = new Intent(Intent.ActionMain);
            intent.AddCategory(Intent.CategoryHome);
            var lst = PackageManager.QueryIntentActivities(intent, 0);
            if (lst.Count != 0)
            {
                foreach (ResolveInfo resolveInfo in lst)
                {
                    var a = resolveInfo.ActivityInfo.PackageName;
                }
            }
        }

        private void LauncherForDefault()
        {
            PackageManager.ClearPackagePreferredActivities(PackageName);

        }

        public override bool OnKeyUp(Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.VolumeDown)
            {
                return true;
            }

            if (keyCode == Keycode.VolumeUp)
            {
                return true;
            }
            return base.OnKeyUp(keyCode, e);
        }

        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.VolumeDown)
            {
                return true;
            }

            if (keyCode == Keycode.VolumeUp)
            {
                return true;
            }
            return base.OnKeyDown(keyCode, e);
        }

    }

}

