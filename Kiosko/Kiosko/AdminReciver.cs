﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.App.Admin;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

public class AdminReciver : DeviceAdminReceiver
{
    
    public override void OnEnabled(Context context, Intent intent)
    {
        Common.showToast(context, "[Device Admin enabled]");
        Common.becomeHomeActivity(context);
    }
    
    public override void OnDisabled(Context context, Intent intent)
    {
        Common.showToast(context, "[Device Admin disabled]");
    }

   
    public override void OnLockTaskModeEntering(Context context, Intent intent,
            String pkg)
    {
        Common.showToast(context, "[Kiosk Mode enabled]");
    }

    
    public override void OnLockTaskModeExiting(Context context, Intent intent)
    {
        Common.showToast(context, "[Kiosk Mode disabled]");
    }
}
