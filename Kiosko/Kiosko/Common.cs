﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.App.Admin;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Kiosko;

public class Common
{
    public static void showToast(Context context, String text)
    {
        Toast.MakeText(context, text, ToastLength.Long).Show();
    }

    public static String getHomeActivity(Context c)
    {
        PackageManager pm = c.PackageManager;
        Intent intent = new Intent(Intent.ActionMain);
        intent.AddCategory(Intent.CategoryHome);
        ComponentName cn = intent.ResolveActivity(pm);
        if (cn != null)
            return cn.FlattenToShortString();
        else
            return "none";
    }

    public static void becomeHomeActivity(Context c)
    {
        ComponentName deviceAdmin = new ComponentName(c, Java.Lang.Class.FromType(typeof(AdminReciver)));
        DevicePolicyManager dpm = (DevicePolicyManager)c.GetSystemService(Context.DevicePolicyService);

        if (!dpm.IsAdminActive(deviceAdmin)) {
            showToast(c, "This app is not a device admin!");
            return;
        }
        if (!dpm.IsDeviceOwnerApp(c.PackageName)) {
            showToast(c, "This app is not the device owner!");
            return;
        }
        IntentFilter intentFilter = new IntentFilter(Intent.ActionMain);
        
        intentFilter.AddCategory(Intent.CategoryDefault);
        intentFilter.AddCategory(Intent.CategoryHome);
        ComponentName activity = new ComponentName(c, Java.Lang.Class.FromType(typeof(MainActivity)));
        dpm.AddPersistentPreferredActivity(deviceAdmin, intentFilter, activity);
        showToast(c, "Home activity: " + getHomeActivity(c));
    }


}
