package md5cea38ec756dfe8f28482e0a807176548;


public class AdminReciver
	extends android.app.admin.DeviceAdminReceiver
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onEnabled:(Landroid/content/Context;Landroid/content/Intent;)V:GetOnEnabled_Landroid_content_Context_Landroid_content_Intent_Handler\n" +
			"n_onDisabled:(Landroid/content/Context;Landroid/content/Intent;)V:GetOnDisabled_Landroid_content_Context_Landroid_content_Intent_Handler\n" +
			"n_onLockTaskModeEntering:(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V:GetOnLockTaskModeEntering_Landroid_content_Context_Landroid_content_Intent_Ljava_lang_String_Handler\n" +
			"n_onLockTaskModeExiting:(Landroid/content/Context;Landroid/content/Intent;)V:GetOnLockTaskModeExiting_Landroid_content_Context_Landroid_content_Intent_Handler\n" +
			"";
		mono.android.Runtime.register ("AdminReciver, Kiosko", AdminReciver.class, __md_methods);
	}


	public AdminReciver ()
	{
		super ();
		if (getClass () == AdminReciver.class)
			mono.android.TypeManager.Activate ("AdminReciver, Kiosko", "", this, new java.lang.Object[] {  });
	}


	public void onEnabled (android.content.Context p0, android.content.Intent p1)
	{
		n_onEnabled (p0, p1);
	}

	private native void n_onEnabled (android.content.Context p0, android.content.Intent p1);


	public void onDisabled (android.content.Context p0, android.content.Intent p1)
	{
		n_onDisabled (p0, p1);
	}

	private native void n_onDisabled (android.content.Context p0, android.content.Intent p1);


	public void onLockTaskModeEntering (android.content.Context p0, android.content.Intent p1, java.lang.String p2)
	{
		n_onLockTaskModeEntering (p0, p1, p2);
	}

	private native void n_onLockTaskModeEntering (android.content.Context p0, android.content.Intent p1, java.lang.String p2);


	public void onLockTaskModeExiting (android.content.Context p0, android.content.Intent p1)
	{
		n_onLockTaskModeExiting (p0, p1);
	}

	private native void n_onLockTaskModeExiting (android.content.Context p0, android.content.Intent p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
